#include <Adafruit_LSM303DLH_Mag.h>
#include <Adafruit_Sensor.h>

#define PUSH_BUTTON   13

//Adafruit_LSM303DLH_Mag_Unified mag = Adafruit_LSM303DLH_Mag_Unified(12345);
//float Pi = 3.14159;

uint8_t push_button();

void setup() {
  Serial.begin(115200);
  pinMode(PUSH_BUTTON, INPUT);
  
  /*if (!mag.begin()) {
    // There was a problem detecting the LSM303 ... check your connections
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while (1)
      ;
  }*/

  pinMode( LED_RED,OUTPUT);
  pinMode( LED_BLUE,OUTPUT);

}

void loop() {
  static uint8_t button = 1;
  

  
  

  /*float heading;

  sensors_event_t event;
  mag.getEvent(&event);
  // Calculate the angle of the vector y,x
  heading = (atan2(event.magnetic.y, event.magnetic.x) * 180) / Pi;
  // Normalize to 0-360
  if (heading < 0) {
    heading = 360 + heading;
  }
  Serial.print("heading: ");
  Serial.println(heading);*/

  if (push_button()==0){
    //test batterie car long push
    //blink red 4 times
    digitalWrite(LED_RED,HIGH);
    delay(100);
    digitalWrite(LED_RED,LOW);
    delay(100);
    digitalWrite(LED_RED,HIGH);
    delay(100);
    digitalWrite(LED_RED,LOW);
    delay(100);
    digitalWrite(LED_RED,HIGH);
    delay(100);
    digitalWrite(LED_RED,LOW);
    delay(100);
    digitalWrite(LED_RED,HIGH);
    delay(100);
    digitalWrite(LED_RED,LOW);
  }

  if(button != push_button()){

    Serial.println("blinking blue 5 times : button has been pushed, not a long push");
    digitalWrite(LED_BLUE,HIGH);
    delay(100);
    digitalWrite(LED_BLUE,LOW);
    delay(100);
    digitalWrite(LED_BLUE,HIGH);
    delay(100);
    digitalWrite(LED_BLUE,LOW);
    delay(100);
    digitalWrite(LED_BLUE,HIGH);
    delay(100);
    digitalWrite(LED_BLUE,LOW);
    delay(100);
    digitalWrite(LED_BLUE,HIGH);
    delay(100);
    digitalWrite(LED_BLUE,LOW);
    delay(100);
    digitalWrite(LED_BLUE,HIGH);
    delay(100);
    digitalWrite(LED_BLUE,LOW);
    delay(100);


    
    button = push_button();
    Serial.print("button: ");
    Serial.println(button);
    
    //to let know the user the configuration of the button
    for (int i = 0; i < button ; i++)
    {
      //Serial.println(i+1);
      last_vibrate_time = millis();
      digitalWrite(LED_RED,HIGH);
      delay(100);
      digitalWrite(LED_RED,LOW);
      
      //vibrate(MOTOR0, 1);
      delay(200);
    }
  }
  
  
}

uint8_t push_button()
{
  static int buttonPushCounter = 1;
  static int buttonState;
  static int lastButtonState = LOW;
  static unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
  static unsigned long lastPressedTime = 1001;
  static bool longpress = true;
  int reading = digitalRead(13);
  bool long_press_state = false;

  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > 60)  {
    if ((millis() - lastDebounceTime) > 1000 && buttonState == HIGH && longpress == true)
    {
      Serial.println("Long presssssssssssssssssss");
      longpress = false;
      long_press_state = true;
    }
    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;

      if (longpress == false)
      {
        longpress = true;
      }
      else if (buttonState == LOW) {
        lastPressedTime = millis();
        Serial.println("Button pressed!: ");
        buttonPushCounter++;
        lastPressedTime = millis();
      }
    }
  }
  if (buttonPushCounter > 5)
  {
    buttonPushCounter = 1;
  }
  lastButtonState = reading;

  if (long_press_state)
  {
    long_press_state = false;
    return 0;
  }
  else
    return buttonPushCounter;
}
