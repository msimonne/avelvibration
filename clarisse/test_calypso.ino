#include <bluefruit.h>
#include <Wire.h>

struct Calypso_data {
  int wind_direction;
  float wind_speed;
  int battery_level;
  int temperature;
  int roll;
  int pitch;
  int ecompass;
} ;

bool Calypso_connected = false;
Calypso_data wind;

BLEClientService data_service(0x180D);
BLEClientService device_information(0x180A);
BLEClientCharacteristic data_characteristic(0x2A39);

void setup_BLE();
void scan_callback(ble_gap_evt_adv_report_t* report);
void connect_callback(uint16_t conn_handle);
void disconnect_callback(uint16_t conn_handle, uint8_t reason);
void data_notify_callback(BLEClientCharacteristic* chr, uint8_t* buffer_data, uint16_t len);

void setup() {
  Serial.begin(115200);
  Serial.print("Connecting the Calypso\n");
  setup_BLE();
  Serial.println("Scanning ...");
  delay(500);

  pinMode( LED_RED,OUTPUT);
  pinMode( LED_BLUE,OUTPUT);
}

void loop() {
  /*Serial.print("Direction : ");
  Serial.println(wind.wind_direction);    //OK
  Serial.print("Battery : ");
  Serial.println(wind.battery_level);     //OK
  Serial.print("Speed : ");
  Serial.println(wind.wind_speed);        //OK
  Serial.print("Temperature : ");
  Serial.println(wind.temperature);       //OK
  Serial.print("Roll : ");
  Serial.println(wind.roll);              //NIET
  Serial.print("Pitch : ");
  Serial.println(wind.pitch);             //NIET*/
  Serial.print("Compass : ");
  Serial.println(wind.ecompass);          //Encore plus NIET
  delay(500);

}

////Code Victor : Fonctions connection Calypso ////////////////////////////////////////////////////////////////////
void scan_callback(ble_gap_evt_adv_report_t* report)
{
  // we filter the scan to devices having only services that Calypso have  
  if (Bluefruit.Scanner.checkReportForService(report, data_service)) 
  {
    Serial.println("Calypso found!");
    Serial.printBufferReverse(report->peer_addr.addr, 6, ':');
    Serial.println(" ");
    Bluefruit.Central.connect(report);
  }
  else
  {
    Serial.println("Device found but not Calypso...");
    Bluefruit.Scanner.resume();
  }
}
void connect_callback(uint16_t conn_handle)
{  

  Serial.println("Connected");
  // once the device is connected, we check if we can access the 
  // service/characteristic 
  Serial.print("Discovering Datas Service ... ");
  // If DATA is not found, disconnect and return
  if ( !data_service.discover(conn_handle) )
  {
    Serial.println("Found NONE");
    // disconnect since we couldn't find DATA service
    Bluefruit.disconnect(conn_handle);
    return;
  }
  Serial.println("Found it");
  
  Serial.print("Discovering device information ... ");
  // If DATA is not found, disconnect and return
 

  // Once DATA service is found, we continue to discover its characteristic
  Serial.print("Discovering Data characteristic ... ");
  if ( !data_characteristic.discover() )
  {
    Serial.println("not found !!!");  
    Serial.println("Measurement characteristic is mandatory but not found");
    Bluefruit.disconnect(conn_handle);
    return;
  }
  Serial.println("Found it");
  Serial.println("Ready!");
  if (data_characteristic.enableNotify())
  {
    Serial.println("Notifications enabled");  
    Calypso_connected = true;
    //vibrate(MOTOR0, 14);
    //blink blue+red 2 times
    Serial.print("Blinking blue+red twice : Calypso is connected");
    digitalWrite(LED_RED,HIGH);
    digitalWrite(LED_BLUE,HIGH);
    delay(500);
    digitalWrite(LED_RED,LOW);
    digitalWrite(LED_BLUE,LOW);
    delay(500);
    digitalWrite(LED_RED,HIGH);
    digitalWrite(LED_BLUE,HIGH);
    delay(500);
    digitalWrite(LED_RED,LOW);
    digitalWrite(LED_BLUE,LOW);
    
    
  }
  else
  {
    Serial.println("Problem when enabling notifications");
    Bluefruit.disconnect(conn_handle);
    return;
  }
}

void disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
  Calypso_connected = false;
  (void) conn_handle;
  (void) reason;
  Serial.print("Disconnected, reason = 0x"); Serial.println(reason, HEX);
}

//Ici on récupère toutes les données du Calypso : nord correcte (entre 0 et 360 exclu),
void data_notify_callback(BLEClientCharacteristic* chr, uint8_t* buffer_data, uint16_t len)
{
  //conversion of the data found on the Calypso
  wind.battery_level = int(buffer_data[4]) * 10;
  wind.wind_speed = (float(buffer_data[1]) * 256 + float(buffer_data[0])) / 100;
  wind.wind_direction = int(buffer_data[3]) * 256 + int(buffer_data[2]);
  wind.temperature = int(buffer_data[5]) - 100;
  wind.roll = int(buffer_data[6]) - 90;
  if (wind.roll == -90)
    wind.roll = 0;
  wind.pitch = int(buffer_data[7]) - 90;
  if (wind.pitch == -90)
    wind.pitch = 0;
  wind.ecompass = 360 - int(buffer_data[9]) * 256 - int(buffer_data[8]);
  if (wind.ecompass == 360)
    wind.ecompass = 0;

}

void setup_BLE()
{
  // Initialize Bluefruit with maximum connections as Peripheral = 0, Central = 1
  Bluefruit.begin(0, 1);
  
  Bluefruit.setName("Haptic_Calypso");
  // Set the connect/disconnect callback handlers
  Bluefruit.Central.setConnectCallback(connect_callback);
  Bluefruit.Central.setDisconnectCallback(disconnect_callback);
  // device_information.begin();
  data_service.begin();

  data_characteristic.setNotifyCallback(data_notify_callback);
  data_characteristic.begin();

  Bluefruit.Scanner.setRxCallback(scan_callback);
  Bluefruit.Scanner.restartOnDisconnect(true);
  Bluefruit.Scanner.filterRssi(-80); 
  Bluefruit.Scanner.setInterval(160, 80);
  //Bluefruit.Scanner.filterMSD();
  Bluefruit.Scanner.useActiveScan(false);
  Bluefruit.Scanner.start(0);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
