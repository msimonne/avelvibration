#include <bluefruit.h>
#include <Adafruit_LSM303DLH_Mag.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>

/////////////////Code Victor : relier + récupérer données du Calypso/////////////////////////////////////////////
struct Calypso_data {
  int wind_direction;
  float wind_speed;
  int battery_level;
  int temperature;
  int roll;
  int pitch;
  int ecompass;
} ;

#define PUSH_BUTTON   13

bool Calypso_connected = false;
Calypso_data wind;

BLEClientService data_service(0x180D);
BLEClientService device_information(0x180A);
BLEClientCharacteristic data_characteristic(0x2A39);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Adafruit_LSM303DLH_Mag_Unified mag = Adafruit_LSM303DLH_Mag_Unified(12345);
float Pi = 3.14159;

void setup_BLE();
void scan_callback(ble_gap_evt_adv_report_t* report);
void connect_callback(uint16_t conn_handle);
void disconnect_callback(uint16_t conn_handle, uint8_t reason);
void data_notify_callback(BLEClientCharacteristic* chr, uint8_t* buffer_data, uint16_t len);
uint8_t push_button();

int get_sector(float heading);
bool same_sector(float heading, int anterior_sector);
void blink_right(void); //blue
void blink_left(void); //red
void blink_wave_left(void); // 3 blue
void blink_wave_right(void); // 3 red




/*float anterior_value_head_north;
float anterior_value_boat_north;
float anterior_value_boat_wind;
float anterior_value_head_wind;

//float anterior_value_date;
int anterior_sector_head;
int anterior_sector_boat;*/

float seuil = 30;

void setup(void) {
  Serial.begin(115200);
  
  pinMode(PUSH_BUTTON, INPUT);
  
  /*Connecte le Calypso*/
  //////////////////////////////Code Victor//////////////////////////
  //Serial.println("Haptic weather vane");
  //Serial.println("-----------------------------------\n");
  setup_BLE();
  //Serial.println("OK");
  //////////////////////////////////////////////////////////////////
  
  

  /* Initialise the sensor */
  if (!mag.begin()) {
    /* There was a problem detecting the LSM303 ... check your connections */
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while (1)
      ;
  }

  // Initializes the LEDs : needs to be a variable "mode_fin = True/False" à set with the push button ?
  pinMode( LED_RED,OUTPUT);
  pinMode( LED_BLUE,OUTPUT);

  /*//Est-ce qu'initialiser tout ça est nécessaire ? pour quoi pas tout mettre à 0 au début ?

  //Initialisation de la valeure anterieure head nord
  sensors_event_t event;
  mag.getEvent(&event);
  float heading = (atan2(event.magnetic.y, event.magnetic.x) * 180) / Pi;
  if (heading < 0) {
    heading = 360 + heading;
  }

  //Initialisation valeur anterieure boat nord
  
  float boating = wind.ecompass;
  
  anterior_value_head_north = heading;
  anterior_value_boat_north = boating;
  //anterior_value_date = millis();

  //Initializes the anterior sector
  anterior_sector_head = get_sector(heading);
  anterior_sector_boat = get_sector(boating);
  */
  
  
  
}

void loop(void) {

  //On décide d'utiliser le nombre de push sur le bouton pour changer de mode vent/nord, 
  //mode fin dejà enclenché
  static uint8_t button = 1;

  static unsigned long last_vibrate_time = 0;

  //On initialise tout ici
  float heading;
  float boating;
  float wind_boat;
  float wind_head;
  static float anterior_value_head_north = 0;
  static float anterior_value_boat_north = 0;
  static float anterior_value_head_wind = 0;
  static float anterior_value_boat_wind = 0;
  static int anterior_sector_head_north = 0;
  static int anterior_sector_boat_north = 0;
  static int anterior_sector_head_wind = 0;
  static int anterior_sector_boat_wind = 0;
  

  if (Calypso_connected== false && (abs(millis() - last_vibrate_time) > 2000))
   {
     //vibrate(MOTOR0, 1);
     digitalWrite(LED_RED,HIGH);
     digitalWrite(LED_BLUE,HIGH);
     delay(800);
     digitalWrite(LED_RED,LOW);
     digitalWrite(LED_BLUE,LOW);
     last_vibrate_time = millis();
   }
  
  //Est-ce qu'on s'embete à tout calculer à chaque fois ?
  //Ou juste les variables du mode ?
  
  
  // HOMME/NORD                            heading
  
  sensors_event_t event;
  mag.getEvent(&event);
  // Calculate the angle of the vector y,x
  heading = (atan2(event.magnetic.y, event.magnetic.x) * 180) / Pi;
  // Normalize to 0-360
  if (heading < 0) {
    heading = 360 + heading;
  }
  //Serial.print("heading: ");
  //Serial.println(heading);


  //BATEAU/NORD                            boating
  
  boating = wind.ecompass;
  //Serial.print("boating: ");
  //Serial.println(boating);

  //VENT/BATEAU                            wind_boat
  
  wind_boat = wind.wind_direction;
  //Serial.print("wind_boat: ");
  //Serial.println(wind_boat);

  //VENT/HOMME                             wind_head
  
  wind_head = heading - boating;
  if(wind_head<0){
    wind_head= 360 + wind_head;
  }
  //Serial.print("wind_head: ");
  //Serial.println(wind_head);

  //Serial.print("button: ");
  //Serial.println(button);



  if (push_button()==0){
    //test batterie car long push
    Serial.println("LONG PUSH");
    digitalWrite(LED_RED,HIGH);
    digitalWrite(LED_BLUE,HIGH);
    delay(500);
    digitalWrite(LED_RED,LOW);
    digitalWrite(LED_BLUE,LOW);
    delay(500);
    Serial.println("Long push : give battery");
    Serial.print("Battery : ");
    Serial.println(wind.battery_level);
    
  }

  if(button != push_button()){
    
    button = push_button();
    Serial.print("button was pushed, we are in case : ");
    Serial.println(button);
    
    //to let know the user the configuration of the button
    for (int i = 0; i < button ; i++)
    {
      last_vibrate_time = millis();
      //vibrate(MOTOR0, 1);
      delay(200);
    }
  }

  // keep the belt from vibrating all the time : limit to 2,5s between each vibration
  if (Calypso_connected && (abs(millis() - last_vibrate_time) > 2500)){
    
    
    switch(button){
      
      case 1:
      
        //Mode indique vent par rapport au bateau
      
        /*Serial.println("vent / bateau ");
        //1)Verifier le changement de secteur
        if(not same_sector(wind_boat,anterior_sector_boat_wind)){
          
          Serial.print("blink right");
          //2)Si oui : vibrer moteur du secteur
          anterior_value_boat_wind = wind_boat;
          anterior_sector_boat_wind = get_sector(wind_boat);
          //vibrate motor sector
          blink_right();

          //3)set last_vibrating_time 
          last_vibrate_time = millis();
          
        }*/
        
        //nord par rapport bateau

        Serial.println(" North / Boat ");
        
        if(not same_sector(boating,anterior_sector_boat_north)){
          Serial.println("Changement de secteur");
          //2)Si oui : vibrer moteur du secteur
          anterior_value_boat_north = boating;
          anterior_sector_boat_north = get_sector(boating);
          //vibrate motor sector

          //3)set last_vibrating_time 
          last_vibrate_time = millis();
        }
        

        
        break;
      case 2:
      
        //Mode indique vent par rapport au bateau + fin
        
        /*Serial.println("vent / bateau + fin");
        //1)Verifier variation du seuil fin
        if (wind_boat - anterior_value_boat_wind > seuil && same_sector(wind_boat,anterior_sector_boat_wind)){
          anterior_value_boat_wind = wind_boat;
          last_vibrate_time = millis();
          //vibrate vers la droite
          blink_wave_right();
          
        }

        if (wind_boat - anterior_value_boat_wind < -seuil && same_sector(wind_boat,anterior_sector_boat_wind)){
          anterior_value_boat_wind = wind_boat;
          last_vibrate_time = millis();
          //vibrate vers la gauche
          blink_wave_left();
        }
        
        //2)Si changement de cadran vibrer normalement
        if(not same_sector(wind_boat,anterior_sector_boat_wind)){
          anterior_value_boat_wind=wind_boat;
          anterior_sector_boat_wind = get_sector(wind_boat);
          last_vibrate_time = millis();
          blink_left();
        }*/

        //Nord/Bateau + fin

        Serial.println(" North / Boat + fin ");

        //Si changement de cadran vibrer normalement
        
        if(not same_sector(boating,anterior_sector_boat_north)){
          anterior_value_boat_north=boating;
          anterior_sector_boat_north = get_sector(boating);
          last_vibrate_time = millis();
        }

        //Pas de changement de secteur : Verifier variation du seuil fin
        if (boating - anterior_value_boat_north > seuil){
          anterior_value_boat_north = boating;
          last_vibrate_time = millis();
          //vibrate vers la droite
        }

        if (boating - anterior_value_boat_north < -seuil ){
          anterior_value_boat_north = boating;
          last_vibrate_time = millis();
          //vibrate vers la gauche
        }
        
        
        
        
        
        break;
      case 3:
      
        //Mode indique le vent / tête
        
        /*Serial.println("vent / head");
        if(not same_sector(wind_head,anterior_sector_head_wind)){
          anterior_sector_head_wind = get_sector(wind_head);
          anterior_value_head_wind = wind_head;
          last_vibrate_time = millis();
        }*/

        // Nord / Tête

        Serial.println(" North / Head ");
        
        Serial.print(" Heading : ");
        Serial.println(heading);
        
        Serial.print(" Anterior heading value : ");
        Serial.println(anterior_value_head_north);
        
        Serial.print(" Sector : ");
        Serial.println(get_sector(heading));
        
        Serial.print(" Anterior Sector : ");
        Serial.println(anterior_sector_head_north);
        
        
        if(not same_sector(heading,anterior_sector_head_north)){
          Serial.println("Changement de secteur");
          anterior_sector_head_north = get_sector(heading);
          anterior_value_head_north = heading;
          last_vibrate_time = millis();
        }
        
        break;
      case 4:
        
        //Mode indique vent / tête + fin
        
        /*Serial.println("vent / head + fin ");
         * 
        if(not same_sector(wind_head,anterior_sector_boat_wind)){
          anterior_value_head_wind=wind_head;
          anterior_sector_head_wind = get_sector(wind_head);
          last_vibrate_time = millis();
        }
          
        if (wind_head - anterior_value_head_wind > seuil){
          anterior_value_head_wind = wind_head;
          last_vibrate_time = millis();
          //vibrate vers la droite
          blink_wave_right();
        }

        if (wind_head - anterior_value_head_wind < -seuil){
          anterior_value_head_wind = wind_head;
          last_vibrate_time = millis();
          //vibrate vers la gauche
          blink_wave_left();
        }
        */

        // Nord / Tête + fin

        Serial.println(" North / Head + fin ");
        Serial.print(" Heading : ");
        Serial.println(heading);

        if(not same_sector(heading,anterior_sector_head_north)){
          anterior_sector_head_north = get_sector(heading);
          anterior_value_head_north = heading;
          last_vibrate_time = millis();
          Serial.println("Changement de secteur");
        }

        if (heading - anterior_value_head_north > seuil){
          anterior_value_head_north = heading;
          last_vibrate_time = millis();
          //vibrate vers la droite
          Serial.println("Petite varation vers la droite");
        }

        if (heading - anterior_value_head_north < -seuil){
          anterior_value_head_north = heading;
          last_vibrate_time = millis();
          Serial.println("Petite varation vers la gauche");
          //vibrate vers la gauche
        }

        
        break;

    }
  }





  
  /*if (heading - anterior_value_north > seuil && same_sector(heading,anterior_sector)){
    
    anterior_value_north = heading;
    
    //anterior_value_date = millis();
    //blink 3 times for the wave but careful if not changing motor sector
    digitalWrite(LED_RED,HIGH);
    delay(500);
    digitalWrite(LED_RED,LOW);
    delay(500);
    digitalWrite(LED_RED,HIGH);
    delay(500);
    digitalWrite(LED_RED,LOW);
    delay(500);
    digitalWrite(LED_RED,HIGH);
    delay(500);
    digitalWrite(LED_RED,LOW);
    delay(500);
  }
  if (heading - anterior_value_north < -seuil && same_sector(heading,anterior_sector)){
    anterior_value_north = heading;
    
    //anterior_value_date = millis();
    //blink 3 times for the wave but careful if not changing motor sector
    digitalWrite(LED_BLUE,HIGH);
    delay(500);
    digitalWrite(LED_BLUE,LOW);
    delay(500);
    digitalWrite(LED_BLUE,HIGH);
    delay(500);
    digitalWrite(LED_BLUE,LOW);
    delay(500);
    digitalWrite(LED_BLUE,HIGH);
    delay(500);
    digitalWrite(LED_BLUE,LOW);
    delay(500);
  }*/
  
  
  

  
  //Serial.print("Compass Heading: ");
  //Serial.println(heading);
  delay(250);
}


void blink_wave_right(void){

  digitalWrite(LED_RED,HIGH);
  delay(500);
  digitalWrite(LED_RED,LOW);
  delay(500);
  digitalWrite(LED_RED,HIGH);
  delay(500);
  digitalWrite(LED_RED,LOW);
  delay(500);
  digitalWrite(LED_RED,HIGH);
  delay(500);
  digitalWrite(LED_RED,LOW);
  delay(500);
}

void blink_wave_left(void){
  digitalWrite(LED_BLUE,HIGH);
  delay(500);
  digitalWrite(LED_BLUE,LOW);
  delay(500);
  digitalWrite(LED_BLUE,HIGH);
  delay(500);
  digitalWrite(LED_BLUE,LOW);
  delay(500);
  digitalWrite(LED_BLUE,HIGH);
  delay(500);
  digitalWrite(LED_BLUE,LOW);
  delay(500);
}

void blink_right(void){
  digitalWrite(LED_RED,HIGH);
  delay(1000);
  digitalWrite(LED_RED,LOW);
  delay(100);
}
void blink_left(void){
  digitalWrite(LED_BLUE,HIGH);
  delay(1000);
  digitalWrite(LED_BLUE,LOW);
  delay(100);
}

uint8_t push_button(){
  static int buttonPushCounter = 3;
  static int buttonState;
  static int lastButtonState = LOW;
  static unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
  static bool longpress = true;
  bool long_press_state = false;

  //ATTENTION CONNECTER LE BOUTON POUR TESTER
  int reading = digitalRead(PUSH_BUTTON);

  if (reading != lastButtonState) {
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > 60)  {
    if ((millis() - lastDebounceTime) > 1000 && buttonState == HIGH && longpress == true)
    {
      Serial.println("Lonnnnnnnnnng press");
      longpress = false;
      long_press_state = true;
    }
    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;

      if (longpress == false)
      {
        longpress = true;
      }
      else if (buttonState == LOW) {
        Serial.println("Short press");
        buttonPushCounter++;
      }
    }
  }
  //ICI NOMBRE DE PUSH MAX ==> 4 pour les 4 modes différents
  if (buttonPushCounter > 4)
  {
    buttonPushCounter = 1;
  }
  lastButtonState = reading;

  if (long_press_state)
  {
    long_press_state = false;
    return 0;
  }
  else
    return buttonPushCounter;
}

bool same_sector(float heading, int anterior_sector){

  //heading appartient à [0,360[ normalement
  int sector = get_sector(heading);
  if (sector!= anterior_sector){
    return false;
  }
  return true;
}
// apparemment wind n'a pas besoin d'être pris en paramètre ? je sais paaaas


int get_sector(float heading){
  int sector;
  if (heading < 18,0 || heading >= 342,0 ){
    sector = 1;
    }  
    
  else if (heading < 54,0){
    //motor 2 [18,54[
    sector=2;
    
    }
  else if (heading <90,0){
    //motor 3 [54,90[
    sector=3;
    }
  else if (heading<126,0){
    //motor 4 [90,126[
    sector=4;
    }
  else if (heading<162,0){
    //motor 5 [126,162[
    sector=5;
    }
  else if (heading<198,0){
    //motor 6 [162,198[
    sector=6;
    }
  else if (heading<234,0){
    sector=7;
    }
  else if (heading<270,0){
    sector=8;
    }
  else if (heading<306,0){
    sector=9;
    }
  else if (heading<342,0){
    sector=10;
    }
  return(sector);
  
}

////Code Victor : Fonctions connection Calypso ////////////////////////////////////////////////////////////////////
void scan_callback(ble_gap_evt_adv_report_t* report)
{
  // we filter the scan to devices having only services that Calypso have  
  if (Bluefruit.Scanner.checkReportForService(report, data_service)) 
  {
    Serial.println("Calypso found!");
    Serial.printBufferReverse(report->peer_addr.addr, 6, ':');
    Serial.println(" ");
    Bluefruit.Central.connect(report);
  }
  else
  {
    Serial.println("Device found but not Calypso...");
    Bluefruit.Scanner.resume();
  }
}
void connect_callback(uint16_t conn_handle)
{  

  Serial.println("Connected");
  // once the device is connected, we check if we can access the 
  // service/characteristic 
  Serial.print("Discovering Datas Service ... ");
  // If DATA is not found, disconnect and return
  if ( !data_service.discover(conn_handle) )
  {
    Serial.println("Found NONE");
    // disconnect since we couldn't find DATA service
    Bluefruit.disconnect(conn_handle);
    return;
  }
  Serial.println("Found it");
  
  Serial.print("Discovering device information ... ");
  // If DATA is not found, disconnect and return
 

  // Once DATA service is found, we continue to discover its characteristic
  Serial.print("Discovering Data characteristic ... ");
  if ( !data_characteristic.discover() )
  {
    Serial.println("not found !!!");  
    Serial.println("Measurement characteristic is mandatory but not found");
    Bluefruit.disconnect(conn_handle);
    return;
  }
  Serial.println("Found it");
  Serial.println("Ready!");
  if (data_characteristic.enableNotify())
  {
    Serial.println("Notifications enabled");  
    Calypso_connected = true;
    //vibrate(MOTOR0, 14);
    //blink blue+red 2 times
    digitalWrite(LED_RED,HIGH);
    digitalWrite(LED_BLUE,HIGH);
    delay(500);
    digitalWrite(LED_RED,LOW);
    digitalWrite(LED_BLUE,LOW);
    delay(500);
    digitalWrite(LED_RED,HIGH);
    digitalWrite(LED_BLUE,HIGH);
    delay(500);
    digitalWrite(LED_RED,LOW);
    digitalWrite(LED_BLUE,LOW);
    delay(500);
    digitalWrite(LED_RED,HIGH);
    digitalWrite(LED_BLUE,HIGH);
    delay(500);
    digitalWrite(LED_RED,LOW);
    digitalWrite(LED_BLUE,LOW);
    delay(500);
    digitalWrite(LED_RED,HIGH);
    digitalWrite(LED_BLUE,HIGH);
    delay(500);
    digitalWrite(LED_RED,LOW);
    digitalWrite(LED_BLUE,LOW);
    
  }
  else
  {
    Serial.println("Problem when enabling notifications");
    Bluefruit.disconnect(conn_handle);
    return;
  }
}

void disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
  Calypso_connected = false;
  (void) conn_handle;
  (void) reason;
  Serial.print("Disconnected, reason = 0x"); Serial.println(reason, HEX);
}

//Ici on récupère toutes les données du Calypso : nord correcte (entre 0 et 360 exclu),
void data_notify_callback(BLEClientCharacteristic* chr, uint8_t* buffer_data, uint16_t len)
{
  //conversion of the data found on the Calypso
  wind.battery_level = int(buffer_data[4]) * 10;
  wind.wind_speed = (float(buffer_data[1]) * 256 + float(buffer_data[0])) / 100;
  wind.wind_direction = int(buffer_data[3]) * 256 + int(buffer_data[2]);
  wind.temperature = int(buffer_data[5]) - 100;
  wind.roll = int(buffer_data[6]) - 90;
  if (wind.roll == -90)
    wind.roll = 0;
  wind.pitch = int(buffer_data[7]) - 90;
  if (wind.pitch == -90)
    wind.pitch = 0;
  wind.ecompass = 360 - int(buffer_data[9]) * 256 - int(buffer_data[8]);
  if (wind.ecompass == 360)
    wind.ecompass = 0;

}

void setup_BLE()
{
  // Initialize Bluefruit with maximum connections as Peripheral = 0, Central = 1
  Bluefruit.begin(0, 1);
  
  Bluefruit.setName("Haptic_Calypso");
  // Set the connect/disconnect callback handlers
  Bluefruit.Central.setConnectCallback(connect_callback);
  Bluefruit.Central.setDisconnectCallback(disconnect_callback);
  // device_information.begin();
  data_service.begin();

  data_characteristic.setNotifyCallback(data_notify_callback);
  data_characteristic.begin();

  Bluefruit.Scanner.setRxCallback(scan_callback);
  Bluefruit.Scanner.restartOnDisconnect(true);
  Bluefruit.Scanner.filterRssi(-80); 
  Bluefruit.Scanner.setInterval(160, 80);
  //Bluefruit.Scanner.filterMSD();
  Bluefruit.Scanner.useActiveScan(false);
  Bluefruit.Scanner.start(0);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
