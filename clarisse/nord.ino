#include <Adafruit_NeoPixel.h>
#include <Adafruit_LSM303DLH_Mag.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>


Adafruit_LSM303DLH_Mag_Unified mag = Adafruit_LSM303DLH_Mag_Unified(12345);
float Pi = 3.14159;

float anterior_value_north;
//float anterior_value_date;
int anterior_sector;
float seuil = 30;

void setup(void) {
  Serial.begin(115200);
  Serial.println("Magnetometer Test");
  Serial.println("");

  /* Initialise the sensor */
  if (!mag.begin()) {
    /* There was a problem detecting the LSM303 ... check your connections */
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while (1)
      ;
  }

  // Initializes the LEDs : needs to be a variable "mode_fin = True/False" à set with the push button ?
  pinMode( LED_RED,OUTPUT);
  pinMode( LED_BLUE,OUTPUT);

  
  sensors_event_t event;
  mag.getEvent(&event);
  float heading = (atan2(event.magnetic.y, event.magnetic.x) * 180) / Pi;
  if (heading < 0) {
    heading = 360 + heading;
  }
  anterior_value_north = heading;
  //anterior_value_date = millis();

  //Initializes the anterior sector
  if (heading < 18,0 || heading >= 342,0 ){
    anterior_sector = 1;
    }  
    
  else if (heading < 54,0){
    //motor 2 [18,54[
    anterior_sector=2;
    
    }
  else if (heading <90,0){
    //motor 3 [54,90[
    anterior_sector=3;
    }
  else if (heading<126,0){
    //motor 4 [90,126[
    anterior_sector=4;
    }
  else if (heading<162,0){
    //motor 5 [126,162[
    anterior_sector=5;
    }
  else if (heading<198,0){
    //motor 6 [162,198[
    anterior_sector=6;
    }
  else if (heading<234,0){
    anterior_sector=7;
    }
  else if (heading<270,0){
    anterior_sector=8;
    }
  else if (heading<306,0){
    anterior_sector=9;
    }
  else if (heading<342,0){
    anterior_sector=10;
    }
  
}

void loop(void) {
  /* Get a new sensor event */
  sensors_event_t event;
  mag.getEvent(&event);

  //float Pi = 3.14159;

  // Calculate the angle of the vector y,x
  float heading = (atan2(event.magnetic.y, event.magnetic.x) * 180) / Pi;

  // Normalize to 0-360
  if (heading < 0) {
    heading = 360 + heading;
  }

  //compares to the constant value
  // NEED TO WAIT BEFORE EMITING NEW SIGNAL => delay at the end of the loop or check with time constant ?
  // NO WAVE IF PASS TO ANOTHER SECTOR
  
  if (heading - anterior_value_north > seuil && same_sector(heading,anterior_sector)){
    anterior_value_north = heading;
    //anterior_value_date = millis();
    
    //blink 3 times for the wave but careful if not changing motor sector
    digitalWrite(LED_RED,HIGH);
    delay(500);
    digitalWrite(LED_RED,LOW);
    delay(500);
    digitalWrite(LED_RED,HIGH);
    delay(500);
    digitalWrite(LED_RED,LOW);
    delay(500);
    digitalWrite(LED_RED,HIGH);
    delay(500);
    digitalWrite(LED_RED,LOW);
    delay(500);
  }
  if (heading - anterior_value_north < -seuil && same_sector(heading,anterior_sector)){
    anterior_value_north = heading;
    //anterior_value_date = millis();
    
    //blink 3 times for the wave but careful if not changing motor sector
    digitalWrite(LED_BLUE,HIGH);
    delay(500);
    digitalWrite(LED_BLUE,LOW);
    delay(500);
    digitalWrite(LED_BLUE,HIGH);
    delay(500);
    digitalWrite(LED_BLUE,LOW);
    delay(500);
    digitalWrite(LED_BLUE,HIGH);
    delay(500);
    digitalWrite(LED_BLUE,LOW);
    delay(500);
  }
  
  
  

  
  //Serial.print("Compass Heading: ");
  //Serial.println(heading);
  delay(250);
}

bool same_sector(float heading, int anterior_sector){

  //heading appartient à [0,360[ normalement

  //first motor [342;18[
  if (heading < 18,0 || heading >= 342,0 ){
    if ( anterior_sector == 1){
      return true;
    }else{
      return false;
    }  
    
  }else if (heading < 54,0){
    //motor 2 [18,54[
    if( anterior_sector == 2){
      return true;
    }else{
      return false;
    }
    
  }else if(heading <90,0){
    //motor 3 [54,90[
    if(anterior_sector == 3){
      return true;
    }else{
      return false;
    }
  }else if(heading<126,0){
    //motor 4 [90,126[
    if(anterior_sector == 4){
      return true;
    }else{
      return false;
    }
  }else if(heading<162,0){
    //motor 5 [126,162[
    if(anterior_sector == 5){
      return true;
    }else{
      return false;
    }
  }else if(heading<198,0){
    //motor 6 [162,198[
    if(anterior_sector == 6){
      return true;
    }else{
      return false;
    }
  }else if(heading<234,0){
    if(anterior_sector == 7){
      return true;
    }else{
      return false;
    }
  }else if(heading<270,0){
    if(anterior_sector == 8){
      return true;
    }else{
      return false;
    }
  }else if(heading<306,0){
    if(anterior_sector == 9){
      return true;
    }else{
      return false;
    }
  }else if(heading<342,0){
    if(anterior_sector == 10){
      return true;
    }else{
      return false;
    }
  }
    
}
